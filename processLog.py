
from os import abort


if __name__=="__main__":
    from collections import defaultdict
    f = open('unconfMuonTools.txt', 'r')
    result=defaultdict(list)
    # Format assumed is
    # S:0 E: ToolSvc   ERROR  CP::MuonSelectionTool/MuonCreatorAlg.MuonCreatorTool.MuonSelectionTool : Explicitly named tools are required to be configured!
    for line in f:
        try:
            tool = line.split()[4].split('/')[0]
            result[tool].append(line)
        except IndexError:
            print ('Failing on this line:')
            print (line)
            abort()

    scores={}
    ignore_list = ['Trk::ResidualPullCalculator', 'Trk::KalmanUpdator', 'Trk::RungeKuttaIntersector', 'Trk::StraightLineIntersector', 'Rec::MuonMomentumBalanceSignificanceTool', 'MuonScatteringAngleSignificanceTool', 'Rec::MuidCaloMaterialParam']
    for key, value in result.items():
        if key not in ignore_list:
           scores[len(value)] = key

    for key in reversed(sorted(scores)):
        print( key, scores[key])

    #max_key = max(result, key= lambda x: len(set(result[x])))
    #print ('Worst issue:', max_key, 'with',len(set(result[max_key])))
    # print (result[max_key])

