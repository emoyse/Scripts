import pickle
top_level_dependencies={}
with open('dependencies.txt', 'rb') as handle:
    top_level_dependencies = pickle.loads(handle.read())

simplified={}
for container,deps in top_level_dependencies.iteritems():
    simplified[container] = sorted(list({ x.split('/')[0]  for x in deps }))

print 'Control', simplified['Control']
    
# topological_sort copied from here: http://stackoverflow.com/questions/11557241/python-sorting-a-dependency-list
def topological_sort(items):
    """
    'items' is an iterable of (item, dependencies) pairs, where 'dependencies'
    is an iterable of the same type as 'items'.

    If 'items' is a generator rather than a data structure, it should not be
    empty. Passing an empty generator for 'items' (zero yields before return)
    will cause topological_sort() to raise TopologicalSortFailure.

    An empty iterable (e.g. list, tuple, set, ...) produces no items but
    raises no exception.
    """
    
    provided = set()
    while items:
         remaining_items = []
         emitted = False
         for item, dependencies in items:
             print 'item', item, 'provided', provided
             if provided.issuperset(dependencies):
                   # so every element in 'dependencies' is in 'provided'
                   yield item
                   provided.add(item)
                   emitted = True
             else:
                   remaining_items.append( (item, dependencies) )
         if not emitted:
             print 'Failed with remaining_items=',remaining_items
             raise TopologicalSortFailure()
         items = remaining_items

x=topological_sort(simplified.iteritems())

for container in x:
    print container
