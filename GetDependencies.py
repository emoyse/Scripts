import os,pprint
veto_list = ['PUBLIC','PRIVATE','atlas_depends_on_subdirs(','${extra_deps}', '${extra_dep}']

package_dependencies = {}
base_dir = '/Users/emoyse/Documents/aogt8/'

for subdir, dirs, files in os.walk(base_dir):
    for file in files:
        if file=='CMakeLists.txt':
            f = open(subdir+'/'+file,'r')
            lines=f.readlines()
            start=bra=ket=False
            dependencies = []
            for line in lines:
                if 'set( extra_dep' in line:
                    # this is a bit of a hack... but want to handle this pattern too if I can
                    temp = line.split()
                    for t in temp:
                        if t not in ['set(',')','extra_deps', 'extra_dep']:
                            dependencies.append(t)
                if 'atlas_depends_on_subdirs' in line:
                    start = True
                if start and '(' in line:
                    bra=True
                if bra:
                    if ')' in line:
                        ket=True
                        break
                    else:
                        # first check if line can be split
                        splitline = line.split()
                        if len(splitline)==1:
                            line = splitline[0] # remove spaces etc
                            if not line in veto_list and not line.startswith('#'):
                                dependencies.append(line)
            rel_path = os.path.relpath(subdir,base_dir)
            package_dependencies[rel_path]=dependencies

pp = pprint.PrettyPrinter(indent=4)   
# pp.pprint( package_dependencies )

# build up top level dependencies
top_level_dependencies = {}
for k,v in package_dependencies.iteritems():
    top_level = k.split('/')[0]
    #skip self references i.e. don't have references to Tracking packages in Tracking containers
    external_dependencies = []
    for dep in v:
        if top_level not in dep.split('/'):
            external_dependencies.append(dep)
        
    if top_level not in top_level_dependencies:
        top_level_dependencies[top_level]=set(external_dependencies)
    else:
        for dep in external_dependencies:
            top_level_dependencies[top_level].add(dep)


# Let's print this, but simplifiy even more:
print '{:<20}{}'.format('Container','Dependencies')
print '{:_<40}'.format('')

simplified = {}
for container,deps in top_level_dependencies.iteritems():
    simplified[container] = sorted(list({ x.split('/')[0]  for x in deps }))
    print '{:<20}{}'.format(container,'\n                    '.join(simplified[container]))



# print '{:<20}{}'.format('Container','Dependencies')
# print '{:_<40}'.format('')
# for container,deps in simplified.iteritems():
#     print '{:<20}{}'.format(container,'\n                    '.join(deps))
#

# to save          
import pickle
with open('dependencies.txt', 'wb') as handle:
  pickle.dump(top_level_dependencies, handle) 
  
# to load
# with open('dependencies.txt', 'rb') as handle:
#     top_level_dependencies = pickle.loads(handle.read())

    
    