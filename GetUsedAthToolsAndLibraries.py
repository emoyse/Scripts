#!/usr/bin/env python
# Use this as GetUsedAthToolsAndLibraries.py logfile
# It assumes you have set up athena with the same version as used to generate logfile
import sys
from collections import defaultdict
 
# Let's find the components file
import os
splitLDPATH = (os.environ['LD_LIBRARY_PATH']).split(':')
pathToComponents = [s for s in splitLDPATH if '/Athena/' in s][0] # Ugh
print ('Using components file found here: '+pathToComponents)
# Format is e.g v2::libMuonPrepRawDataProviderTools.so:Muon::MuonLayerHashProviderTool
componentToLibrary = defaultdict(str)
with open(pathToComponents+'/Athena.components', "r") as ifile:
    for line in ifile:
        tmp = line.split('.so:')
        library = tmp[0]
        library = library[library.find('lib')+3:]
        componentToLibrary[tmp[1].rstrip()]+=library

# Open log file and loop through
toolsLoaded = defaultdict(list)
file_name = sys.argv[1]
print ('Processing the following athena log file '+file_name)
with open(file_name, "r") as ifile:
    for line in ifile:
        if not line:
            break
        if 'TOOL RELATION' in line:
            tmp = line.split()
            
            toolName = tmp[2] 
            # This can be long e.g. Muon::MuonTrackCleaner/MuonSegmentMaker_NCB.Muon::MuonClusterSegmentFinder.DCMathSegmentMaker.MuonSegmentFittingTool.Muon::MuonTrackCleaner
            toolName = toolName[:toolName.find('/')] # Strip 
            
            parent = tmp[3].split('=')[1]
            toolsLoaded[toolName].append(parent)

#Now I want to find the library / package
# Need to loop through the tool list and find 

# Now output in twiki format
outfile = open('twiki.out','w')
outfile.write('%TABLE{ sort="on" initsort="2" }% \n')
outfile.write('| *Tool* | *Package* | *Responsible* | *Thread safe* | *Parent(s) (useful for working out dependencies. ToolSvc=public tool)* | \n')

for toolName, parent in toolsLoaded.items():
    outfile.write('| {tool} | {package} |  |  | {parent} | \n'.format(tool='<nop>'+toolName, package='<nop>'+componentToLibrary[toolName], parent=parent) )

print('fin')