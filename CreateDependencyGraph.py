#!/usr/bin/python
# import CreateDependencyGraph as c
# packages = c.traverseRepo('/Volumes/CaseSensitive/athena/Tracking/TrkEvent/')
# or
# packages = c.traverseRepo()
# G = c.buildGraph(packages)
# c.plot_bokeh(G,externals)


import os,pprint,json
import networkx as nx
from networkx.readwrite import json_graph;

from bokeh.io import show
from bokeh.plotting import figure
from bokeh.models.graphs import from_networkx, NodesAndLinkedEdges, EdgesAndLinkedNodes
from bokeh.models import ColumnDataSource, LabelSet
from bokeh.models import Plot, Range1d, MultiLine, Circle, HoverTool, TapTool, BoxSelectTool, ZoomInTool, ZoomOutTool, PanTool
from bokeh.palettes import Spectral4
    
plot_matlib=False
plot_bokeh=False
# Check SoQt, Gperftools, Lapack
# ShowerDeconstruction - not sure where this comes from. Reconstruction/Jet/JetSubStructureUtils/CMakeLists.txt
atlasexternal_licences = { 'GPLv2': ['HepMCAnalysis', 'FFTW', 'gdb', ], \
                      'GPLv2+': ['FastJet', 'FastJetContrib'], \
                      'GPLv3' : ['KLFitter','Lhapdf'], \
                      'Boost': ['Boost'], \
                      'BSD': ['Gperftools', 'Coin3D', 'dSFMT', 'SoQt'], \
                      'CC-BY 2.0' : ['yaml'], \
                      'Geant': ['Geant4'], \
                      'ISSL' : ['MKL'], \
                      'LGPLv2' : ['Davix', 'ROOT','DCAP'], \
                      'LGPLv3' : ['xRootD', 'BAT'], \
                      'MIT' : ['LibXml2', 'lwtnn'], \
                      'modified BSD' : ['Lapack'], \
                      'MPEG' : ['Simage'], \
                      'MPLv2' : ['Acts','Eigen'], \
                      'PSFv2': ['Python'], \
                      'None' : ['APE', 'yampl'], \
                  }

# For HepPDT I couldn't find a licence except
# http://lcgapp.cern.ch/project/simu/HepPDT/HepPDT.2.06.01/HepPDT.pdf
# It's 'AS IS' which is pretty much MIT? Putting it there for the moment.
# Qt is complicated, since the licence depends on what parts of it we use. I think LGPL is safe though.
# Java is under Oracle Binary Code License. Fine for us.
# MCUtils doesn't have a licence yet, but it will probably be GPLv3
# EvtGenb - couldn't find. Emailed authors.
# CORAL / COOL etc - couldn't find licence so in 'none'
# UoiNCSA Non-restrictive. https://opensource.org/licenses/UoI-NCSA.php
# zlib non-restrictive

# crmc has the following license:
# /***************************************************************************
#  *                                                                         *
#  *  Copyright and any other appropriate legal protection of these          *
#  *  computer programs and associated documentation are reserved in         *
#  *  all countries of the world.                                            *
#  *                                                                         *
#  *  These programs or documentation may not be reproduced by any method    *
#  *  without prior written consent of Karlsruhe Institute of Technology     *
#  *  delegate. Commercial and military use are explicitly forbidden.        *
#  *                                                                         *
#  *  Karlsruhe Institute of Technology welcomes comments concerning the     *
#  *  code but undertakes no obligation for maintenance of the programs,     *
#  *  nor responsibility for their correctness, and accepts no liability     *
#  *  whatsoever resulting from the use of its programs.                     *
#  *                                                                         *
#  ***************************************************************************/

lcg_licences = {'Apachev2':['pyanalysis','GMock','TBB', 'XercesC'], \
                'BSD': ['BLAS', 'Frontier_Client', 'gperftools', 'LAPACK', 'GTest'],\
                'BCL':['Java'], \
                'cmrc' : ['Crmc'], \
                'GPLv2': ['ThePEG', 'Pythia8', 'pygraphics', 'Jimmy', 'valgrind', 'dcap', 'Herwig3', 'MCTester', 'YODA'], \
                'GPLv3':['GSL', 'Rivet', 'Herwig'], \
                'LGPLv2':['glib','HepMC', 'ROOT', 'AIDA', 'CppUnit'], \
                'LGPLv3':['Qt5', 'CLHEP', 'MCUtils', 'Xrootd'], \
                'MIT':['libffi','libxkbcommon', 'HepPDT','pytools', 'CURL', 'EXPAT', 'libunwind'], \
                'None':['CORAL','COOL','CASTOR', 'EvtGen', 'HEPUtils', 'Pythia6','Photospp', 'Photos', 'MCtester', 'Hydjet', 'AlpGen', 'Tauola', 'Tauolapp', 'Starlight', 'Hijing', 'Pyquen', 'Sherpa' ], \
                'Oracle':['Oracle'], \
                'UoiNCSA':['MadGraph'], \
                'zlib' : ['ZLIB']
                }

misc_licences = {'BSD':['OpenGL'], \
                 'MIT':['UUID'], \
                 'None':['tdaq-common', 'tdaq', 'dqm-common', ], \
                }
              
# There are some cases where we use find_package with auto-completed local file. Exclude these.   
find_package_veto = ('PathResolverEnvironment', 'AthenaPoolUtilitiesTest', 'AtlasDataAreaEnvironment', 'CxxUtilsEnvironment', 'AtlasAuthenticationEnvironment', \
                     'VP1AlgsEnvironment', 'Asg_TestEnvironment', 'TwissFilesEnvironment', 'ARTEnvironment', 'AtlasCMake', '${ATLAS_PROJECT}', 'PythonInterp', 'Threads', 'PythonLibs')

gpl_externals = atlasexternal_licences['GPLv2'] + atlasexternal_licences['GPLv2+'] + atlasexternal_licences['GPLv3'] + lcg_licences['GPLv2'] + lcg_licences['GPLv3']

def traverseRepo( base_dir='/Volumes/CaseSensitive/athena', treat_private_as_public=True):
    veto_list = ['PUBLIC','PRIVATE','atlas_depends_on_subdirs(','${extra_deps}', '${extra_dep}']
    packages = {}
    packages_with_direct_dep_on_fastjet = ['xAODJet'] # This is because of FastJetLinkBase.h
    
    for subdir, dirs, files in os.walk(base_dir):
        for file in files:
            if file=='CMakeLists.txt':
                f = open(subdir+'/'+file,'r')
                lines=f.readlines()
                # print 'lines', lines
                package_name=""
                start_internal_deps=bra=False
                internal_public_dependencies = []
                internal_private_dependencies = []
                external_dependencies = []
                public=False # True once we hit public dependencies
                for line in lines:
                    if line.startswith('#'):
                        continue
                    # print line
                    if 'atlas_subdir' in line:
                        package_name = line[line.find("(")+1 : line.find(")")].strip()
                        if 'None' in package_name:
                            # Probably a weird Top packages. Take first bit without 'None'
                            package_name = package_name.split()[0]
                        continue
                    if 'find_package' in line:
                        external = line[line.find("(")+1 : line.find(")")].split()[0]
                        if not external in find_package_veto:
                            external_dependencies.append( external ) # first element inside brackets
                        if 'FastJet' in external:
                            packages_with_direct_dep_on_fastjet.append(package_name)
                        continue
                    if 'set( extra_dep' in line:
                        # this is a bit of a hack... but want to handle this pattern too if I can
                        temp = line.split()
                        for t in temp:
                            if t not in ['set(',')','extra_deps', 'extra_dep']:
                                internal_public_dependencies.append(t)
                        continue
                    if 'atlas_depends_on_subdirs' in line:
                        start_internal_deps = True
                    if start_internal_deps and '(' in line:
                        bra=True
                    if bra:
                        if ')' in line:
                            bra=False
                            start_internal_deps=False
                        else:
                            # first check if line can be split
                            splitline = line.split()
                            if len(splitline)==1:
                                line = splitline[0] # remove spaces etc
                                if 'PUBLIC' in line:
                                    public=True
                                elif 'PRIVATE' in line:
                                    public=True
                                elif not line in veto_list and not line.startswith('#'):
                                    package = line.split('/')[-1].strip()
                                    if public or treat_private_as_public:
                                        internal_public_dependencies.append(package)
                                    else:
                                        internal_private_dependencies.append(package)
                                        
                rel_path = os.path.relpath(subdir,base_dir)
                package = {'external' : external_dependencies, 'internal public' : internal_public_dependencies, 'internal private': internal_private_dependencies}
                packages[package_name]=package
    print 'Scanned ', len(packages), 'packages.'
    return packages

def buildGraph(packages, treat_externals_as_public = True):
    all_external_dependencies = set()
    G = nx.DiGraph()
    for package in packages:
        for external in packages[package]['external']:
            all_external_dependencies.add(external)
            if treat_externals_as_public:
                # print package, external
                G.add_edge(external,package)
        for dependency in packages[package ]['internal public']:
            G.add_edge(dependency, package)
    print 'Got', G.number_of_nodes(), 'nodes.'
    print 'Externals used: ',all_external_dependencies
    unknown_licence = []
    for ext in all_external_dependencies:
        licence_known=False
        for k,v in atlasexternal_licences.items():
            if ext in v:
                print 'atlasexternals package\t', ext,'has licence',k
                licence_known=True
        for k,v in lcg_licences.items():
            if ext in v:
                print 'LCG package\t\t', ext,'has licence',k
                licence_known=True
        for k,v in misc_licences.items():
            if ext in v:
                print 'MISC package\t\t', ext,'has licence',k
                licence_known=True        
                
        if not licence_known:
            unknown_licence.append(ext)
    if unknown_licence:
        print 'We do not knpw the licence for the following:',unknown_licence
    # print 'Nodes', G.nodes()
    return G, all_external_dependencies
# print G.graph
# print 'Edges', G.edges()
# Output it
# d = json_graph.node_link_data(G)
# json.dump(d, open('dependencies.json','w'))

def gpl_infected_packages_from_graph(G, gpl_externals):
    gpl_infected_packages=set()
    for package in gpl_externals:
       successors = nx.descendants(G, package)
       for successor in successors:
           if successor in gpl_externals:
               print 'This is an external?',k
           gpl_infected_packages.add(successor)
    return gpl_infected_packages

# def find_gpl_source(package_to_check, G):
#     gpl_infected_packages = gpl_infected_packages_from_graph(G)
#     for package in nx.ancestors(G, package_to_check):
#      if package in gpl_infected_packages:
#        ancestors = nx.ancestors(G,package)
#        print 'Package',package, 'with '
#        for ancestor in ancestors:
#          if ancestor in gpl_externals:
#            print ancestor
#     return

def prepare_labels(plot, pos):
    print 'prepare_labels pos:', pos
    x, y = zip(*pos.values())
    node_labels = pos.keys()
    print 'node_labels',node_labels
    print 'x',x
    print 'y',y
    
    source = ColumnDataSource({'x': x, 'y': y,
                               'label': [node_labels[i] for i in range(len(x))]})
    labels = LabelSet(x='x', y='y', text='label', source=source)
    print source
    plot.renderers.append(labels)
    return plot

def get_node_pos(G, package, depth, x, pos):
    # First fill the postiion for this one.
    y =  depth*0.2
    # if depth>10:
    #     return
    # print depth, package, x, y
    if package not in pos:
        pos[ package ] = (x,y)
    else:
        # Okay, pretty crude but try to give us a nice starting point here.
        x_old = pos [ package ] [ 0 ]
        y_old = pos [ package ] [ 1 ]
        pos [ package ] = ( ( x+x_old)/2.0, ( y+y_old)/2.0 )
    # Get immediate children of package
    children = list(G.succ[package] )
    # print 'children', children
    x_offset = 0.0
    for child in children:
        get_node_pos(G, child, depth+1, x+x_offset, pos)
        x_offset+=0.2
    return

def atlas_layout(G, externals):
    pos = {}
    x=0.0
    # Draw from exyernals first, to try to get a bit of a tree structure
    for package in externals:
        depth=0
        get_node_pos(G, package, depth, x, pos)
        x+=1.0
    
    depth = 1
    x=-1.0
    for package in G.nodes():
        if package not in pos:
            get_node_pos(G, package, depth, x, pos)
            x+=0.2
    return pos
    
def visualise_graph(G, externals, base_dir='Athena' ):
    plot = figure(title=base_dir, tools="", plot_width=1200, x_range=(-3.5, 3.5),
              y_range=(-   1.5, 1.5))
    plot.add_tools(HoverTool(), TapTool(), BoxSelectTool(), ZoomInTool(), ZoomOutTool(), PanTool())
    package_pos = atlas_layout(G, externals)
    print 'externals', externals
    print 'package_pos:',package_pos
    # k=1/sqrt(len(G.nodes))
    k = 0.01
    graph = from_networkx(G, nx.fruchterman_reingold_layout, pos = package_pos, fixed = externals, iterations=50)
    # graph = from_networkx(G, nx.fruchterman_reingold_layout, pos = package_pos, fixed = externals, iterations=0)
    # graph = from_networkx(G, nx.spring_layout, k=1.0, iterations=100)
    # graph = from_networkx(G, atlas_layout, externals=externals)
    graph.node_renderer.glyph = Circle(size=15, fill_color=Spectral4[0])
    graph.node_renderer.selection_glyph = Circle(size=15, fill_color=Spectral4[2])
    graph.node_renderer.hover_glyph = Circle(size=5, fill_color=Spectral4[1])
    graph.edge_renderer.glyph = MultiLine(line_color="#CCCCCC", line_alpha=0.5, line_width=5)
    graph.edge_renderer.selection_glyph = MultiLine(line_color=Spectral4[2], line_width=5)
    graph.edge_renderer.hover_glyph = MultiLine(line_color=Spectral4[1], line_width=5)
    graph.selection_policy = NodesAndLinkedEdges()
    graph.inspection_policy = NodesAndLinkedEdges()
    pos = graph.layout_provider.graph_layout
    
    plot.renderers.append(graph)
    return plot,pos

def plot_bokeh(G, externals, show_labels=False):  
    plot, pos = visualise_graph(G, externals)
    if show_labels:
        plot = prepare_labels(plot,pos)
    show(plot)
    
def plot_matlib(G, gpl_externals): 
    import matplotlib.pyplot as plt
    pos = nx.spring_layout(G, scale=10.0)
    nx.draw_networkx_nodes(G, pos, node_size = 400)
    nx.draw_networkx_edges(G, pos, arrows=True)
    nx.draw_networkx_labels(G, pos)
    plt.show()
    return
