if __name__=="__main__":
    from collections import defaultdict
    f = open('../../q443_CA/ca/unconfMuonTools.txt', 'r')
    result=defaultdict(list)
    for line in f:
        result[line.split()[2].split('/')[0].split(':')[2]].append(line)
    
    scores={}
    ignore_list = ['ResidualPullCalculator', 'KalmanUpdator']
    for key, value in result.items():
        if key not in ignore_list:
           scores[len(value)] = key

    for key in reversed(sorted(scores)):
        print( key, scores[key])

    #max_key = max(result, key= lambda x: len(set(result[x])))
    #print ('Worst issue:', max_key, 'with',len(set(result[max_key])))
    # print (result[max_key])
