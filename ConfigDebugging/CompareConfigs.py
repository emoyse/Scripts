#!/usr/bin/env python

# To generate the files for this, in the new config it is enough to do e.g. 
# export JOBOPTSDUMPFILE="newconfig.txt"
# With old, you can do:
# Reco_tf.py --inputRDOFile=myRDO.pool.root --outputESDFile=ESD.pool.root --preExec="rec.doMonitoring=False;rec.doTrigger=False ; svcMgr+=CfgMgr.JobOptionsSvc(DUMPFILE = \"oldconfig.txt\")"
def buildConfigurationMap(file_name,args):
    components = {}
    currentComponent=""
    lowpriority = ['DetStore', 'EvtStore', 'OutputLevel', 'ExtraInputs', 'ExtraOutputs', 'NeededResources', 
        'CondSvc', 'AuditInitialize', 'AuditReinitialize', 'AuditFinalize', 'AuditFinalize', 'AuditRestart', 'AuditServices', 'AuditStart', 'AuditStop', 'AutoRetrieveTools']
    with open(file_name, "r") as ifile:
        for line in ifile:
            if not line:
                break
            if '=' in line: # old style
                # e.g. AGDDtoGeoSvc.AuditFinalize = True;
                first = line[:line.find('=')].strip() 
                currentComponent = first[:first.rfind('.')]
                if (currentComponent not in components):
                    components[currentComponent] = {}
                prop = first[first.rfind('.')+1:] # last . to the end
                value = line[line.find('=')+1:].strip(" ;\n") # remove spaces and semicolon from end

                if args.ignoreLowPriority and prop not in lowpriority:
                    components[currentComponent][prop] = value
            else:
                if line.startswith('Client'): # e.g. Client 'AtlasTrackingGeometrySvc'
                    currentComponent = line.split('\'')[1]
                    components[currentComponent] = {}
                else:
                    # e.g. 'ExtraOutputs':[]
                    try:
                        prop = line[:line.find(':')].split("'")[1]
                        value = line[line.find(':')+1:].strip(" \r\n")
                    except IndexError:
                        print('Failed with : '+line)
                        raise
                if args.ignoreLowPriority and prop not in lowpriority:
                    components[currentComponent][prop] = value
                
    return components

if __name__=="__main__":
    import sys, argparse, os
    # from AthenaConfiguration.ComponentFactory import CompFactory
    parser = argparse.ArgumentParser()
    parser.add_argument("--ref",required=True,help="The configuration text file used as reference.")
    parser.add_argument("--chk",required=True,help="The configuration text file to be checked.")
    parser.add_argument("--ignoreMissingComponents",help="Don't report components existing in only one of the two configuartions",action="store_true")
    # parser.add_argument("--ignoreMissingComponents",help="Don't report components existing in only one of the two configuartions",action="store_true")
    parser.add_argument("--ignoreIdentical",help="Only print components with properties which differ, and only print differing properties",action="store_true")
    parser.add_argument("--ignoreLowPriority",help="Ignore differences in e.g. outputlevel",action="store_true", default=True)

    args = parser.parse_args()

    fNameRef=args.ref
    fNameChk=args.chk
    
    for f in (fNameRef,fNameChk):
        if not os.access(f,os.R_OK):
            sys.stderr.write("No read access to file %s\n" % f)
            sys.exit(-1)

    print ('Reference file: ',fNameRef)
    print ('Validation file: ',fNameChk)

    refConfig = buildConfigurationMap(fNameRef, args)
    chkConfig = buildConfigurationMap(fNameChk, args)
    
    # Get list of all keys.
    all_components = sorted( set(list(refConfig.keys()) + list(chkConfig.keys()) ) )

    GREEN = '\033[92m'
    RED = '\033[91m'
    BLUE = '\033[94m'
    ENDCOL = '\033[0m'

    maxHalfWidth=150     # Hardcode for the moment

    print(BLUE+ '{:^{width}}'.format(fNameRef[:maxHalfWidth-2], width=maxHalfWidth)+' | '+'{:^{width}}'.format(fNameChk[:maxHalfWidth-2], width=maxHalfWidth)+ENDCOL)
    print('{:^{width}}'.format('', width=maxHalfWidth)+' | '+'{:^{width}}'.format('', width=maxHalfWidth))
    for component in all_components:
        # if 'MuonTrackingGeometryBuilder' in component:
        #     import pdb ; pdb.set_trace()
     
        # Check simple case that only exists in one or the other.
        if component in chkConfig and component not in refConfig:
            # in chk but not in ref
            if not args.ignoreMissingComponents:
                print(GREEN+'{:<{width}}'.format('', width=maxHalfWidth)+' | '+'{:<{width}}'.format(component[:maxHalfWidth-2], width=maxHalfWidth)+ENDCOL)
                for prop in sorted(chkConfig[component]):

                    propChk = ' - '+(prop+" : "+chkConfig[component][prop])[:maxHalfWidth-5]
                    print(GREEN+'{:<{width}}'.format('', width=maxHalfWidth)+' | '+'{:<{width}}'.format(propChk, width=maxHalfWidth)+ENDCOL)
            continue
        if component in refConfig and component not in chkConfig:
            # in ref but not in chk
            if not args.ignoreMissingComponents:

                print(RED+'{:<{width}}'.format(component[:maxHalfWidth-2], width=maxHalfWidth)+' | '+'{:<{width}}'.format('', width=maxHalfWidth)+ENDCOL)
                for prop in sorted(refConfig[component]):
                    propRef = ' - '+(prop+" : "+refConfig[component][prop])[:maxHalfWidth-5]
                    print(RED+'{:<{width}}'.format(propRef, width=maxHalfWidth)+' | '+'{:<{width}}'.format('', width=maxHalfWidth)+ENDCOL)
            continue
        # Okay, more interesting case now - in both, so compare properties
        printOutput=not args.ignoreIdentical # Print everything if we're not ignoring identical

        output = '{:<{width}}'.format(component[:maxHalfWidth-2], width=maxHalfWidth)+' | '+'{:<{width}}'.format(component[:maxHalfWidth-2], width=maxHalfWidth)+'\n'

        refProps = sorted(refConfig[component])
        chkProps = sorted(chkConfig[component])
        all_properties = sorted( set(refProps + chkProps) )
        for prop in all_properties:
            propChk = ""
            propRef = ""
            if prop in refProps and prop not in chkProps:
                # in ref but not in chk
                propRef = ' - '+(prop+" : "+refConfig[component][prop])[:maxHalfWidth-5]
                output += RED+'{:<{width}}'.format(propRef, width=maxHalfWidth)+' | '+'{:<{width}}'.format('', width=maxHalfWidth)+ENDCOL+'\n'
                printOutput = True
                continue
            if prop in chkProps and prop not in refProps:
                # in chk but not in ref
                propChk = ' - '+(prop+" : "+chkConfig[component][prop])[:maxHalfWidth-5]
                output += GREEN+'{:<{width}}'.format('', width=maxHalfWidth)+' | '+'{:<{width}}'.format(propChk, width=maxHalfWidth)+ENDCOL+'\n'
                printOutput=True
                continue

            # Property exists in both. Now check if the same 
            refValue = refConfig[component][prop]
            chkValue = chkConfig[component][prop]
            propRef = ' - '+(prop+" : "+refValue)[:maxHalfWidth-5]   # This is a bit ugly - setting above and here. 
            propChk = ' - '+(prop+" : "+chkValue)[:maxHalfWidth-5]   # Probably try {} would be cleaner.

            if chkValue != refValue:
                # Okay they're different, but with GaudiConfig2 there are some cosmentic changes. One is that tools now dump type/name whereas beofre it could be just name
                if '/' in refValue:
                    refValue = refValue.split('/')[1]
                if '/' in chkValue:
                    chkValue = chkValue.split('/')[1]
                # Try again
                if chkValue != refValue:
                    output += RED+'{:<{width}}'.format(propRef, width=maxHalfWidth)+' | '+GREEN+'{:<{width}}'.format(propChk, width=maxHalfWidth)+ENDCOL+'\n'
                    printOutput=True
                continue

            if not args.ignoreIdentical:
                output+='{:<{width}}'.format(propRef, width=maxHalfWidth)+' | '+'{:<{width}}'.format(propChk, width=maxHalfWidth)+'\n'
        if printOutput:
            print(output)    
    output += BLUE+ '{:^{width}}'.format(fNameRef[:maxHalfWidth-2], width=maxHalfWidth)+' | '+'{:^{width}}'.format(fNameChk[:maxHalfWidth-2], width=maxHalfWidth)+ENDCOL+'\n'

