#!/usr/bin/env python
if __name__=="__main__":
    import argparse
    import re

    parser = argparse.ArgumentParser()
    parser.add_argument("input",help="The input athena logfile.")
    #parser.add_argument("--ignoreMissingComponents",help="Don't report components existing in only one of the two configuartions",action="store_true")

    args = parser.parse_args()

    print("Processing file "+args.input)

    f = open(args.input,'r')

    currentEvent = None
    currentFile = None
    numEvents = 0
    # Assuming line of type:
    # AthenaHiveEventLoopMgr                                 0     0    INFO   ===>>>  start processing event #225293001, run #284500 on slot 0,  0 events processed so far  <<<===
    for line in f:
        if 'start processing event' in line:
            if currentEvent:
                print('Warning! We have not finished the previous event', currentEvent) 
            currentEvent = re.search('start processing event #(.*), run', line).group(1)
            currentFile=open(str(currentEvent)+'.txt', 'w')
        elif 'done processing event' in line:
            currentEvent=None
            currentFile.close()
            currentFile=None
            numEvents+=1
        if currentFile:
            # Remove any unnecessary differences i.e. pointer output
            line = re.sub('0x[0-9a-fA-F]+','', line)
            currentFile.write(line)
    print('Processed '+str(numEvents)+' events')

