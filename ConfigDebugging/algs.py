decoding_algs = ['StgcRdoToStgcPrepData', 'MuonsTgcRawDataProvider']
segment_algs = ['MuonLayerHoughAlg','MuonSegmentMaker','MuonSegmentCnvAlg', 'QuadNSW_MuonSegmentCnvAlg']
track_making_algs = ['MuPatTrackBuilder','MuonStandaloneTrackParticleCnvAlg']
muon_combined_algs = [ 'MuonCombinedInDetCandidateAlg', 'MuonCombinedMuonCandidateAlg','MuonCombinedAlg', 'MuonInDetToMuonSystemExtensionAlg', 'MuonInsideOutRecoAlg', 'MuonCaloTagAlg']
# algs_to_debug = muon_combined_algs+segment_algs+track_making_algs
# algs_to_debug=['MuPatTrackBuilder']
# algs_to_debug = ['RpcRawDataProvider', 'RpcRdoToRpcPrepData']
algs_to_debug = segment_algs

