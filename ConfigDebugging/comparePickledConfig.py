#!/usr/bin/python

import sys,os,pickle,argparse

def compareConfig(configRef,configChk,compsToCompare=None,ignoreMissing=False,perParameterPrint=False, onlyDiff=False, ignoreIrrelevant=False, printColours=False):

    colours={'RED':'', 'GREEN':'', 'BLUE':'', 'END':''}
    if printColours:
        colours={'RED':'\033[91m ', 'GREEN':'\033[92m', 'BLUE':'\033[94m', 'END':'\033[0m'}
    #Find superset of all components:
    allComps=set(configRef.keys())
    print "Step 1: reference file #components:",len(allComps)
    allComps.update(set(configChk.keys()))
    print "Step 2: file to check  #components:",len(allComps)
                    
    for component in allComps:
        if compsToCompare is not None and len(compsToCompare):
            ignoreThis=True
            for toComp in compsToCompare:
                if component.find(toComp)!=-1:
                    ignoreThis=False
                    break
                pass
            if (ignoreThis): continue
            pass
        

        if not component in configRef:
            if not ignoreMissing: print "Component",component,"exists only in Chk"
            continue

        if not component in configChk:
            if not ignoreMissing: print "Component",component,"exists only in Ref"
            continue
        
        refValue=configRef[component]
        chkValue=configChk[component]
        
        if (chkValue==refValue):
            #print "Component",component,"identical"
            pass
        else:
            if not ignoreMissing: print colours['RED'],"Component",component,"differ",colours['END']
            if perParameterPrint:
                compareComponent(chkValue,refValue,"\t"+component, onlyDiff, ignoreIrrelevant, colours)
            else:
                print colours['GREEN'],"Ref\t",sorted(configRef[component].items(),  key=lambda kv: kv[0]),colours['END']
                print colours['BLUE'],"Chk",sorted(configChk[component].items(), key=lambda kv: kv[0]),colours['END']
    return

def compareComponent(compChk,compRef,prefix="\t", onlyDiff=False, ignoreIrrelevant=False, colours={}):
    allProps=set(compRef.keys())
    allProps.update(set(compChk.keys()))
    
    ignoreList=['StoreGateSvc', 'OutputLevel', 'Muon::MuonEDMHelperSvc/MuonEDMHelperSvc', 'Muon::MuonIdHelperSvc/MuonIdHelperSvc', 'TrackingGeometrySvc/AtlasTrackingGeometrySvc', 'Trk::Extrapolator/MuonExtrapolator']
    for prop in allProps:
        
        if not prop in compRef.keys():
            if not onlyDiff: print "%s.%s exists only in Chk: " % (prefix,prop)
            continue;

        if not prop in compChk.keys():
            if not onlyDiff: print "%s.%s exists only in Ref: " % (prefix,prop)
            continue
        
        chkVal=compChk[prop]
        refVal=compRef[prop]
        
        if chkVal==refVal:
            if onlyDiff:
                continue
            diffmarker=""
        else:
            diffmarker=colours['RED']+" << !!!"+colours['END']

        if ignoreIrrelevant and chkVal in ignoreList:
            continue
        # if chkVal == 'MuonEDMHelperSvc':
        #     import pdb ; pdb.set_trace()
        print "%s.%s : %s[ %s ]%s vs %s[ %s ]%s %s" % (prefix,prop,colours['GREEN'],str(refVal),colours['END'],colours['BLUE'],str(chkVal),colours['END'],diffmarker)


if __name__=="__main__":
    print sys.argv

    parser = argparse.ArgumentParser()
    parser.add_argument("--ref",required=True,help="The configuration pickle file used as reference (obtained by athena --config-only=<file>)")
    parser.add_argument("--chk",required=True,help="The configuration pickle file to be checked(obtained by athena --config-only=<file>)")
    parser.add_argument("--comps",nargs="*",help="Report only component containing this string",action='append')
    parser.add_argument("--ignoreMissing",help="Don't report components existing in only of the two configuartions",action="store_true")
    parser.add_argument("--perParameterPrint",help="print difference for each component property",action="store_true")
    parser.add_argument("--ignoreIdenticalPerParameter",help="Only print difference for each component property, if they differ",action="store_true")
    parser.add_argument("--ignoreIrrelevant",help="Ignore differences in e.g. outputlevel",action="store_true")
    parser.add_argument("--colour",help="Choose whether to output colour or not",action="store_true")

    args = parser.parse_args()

    fNameRef=args.ref
    fNameChk=args.chk
    

    if args.comps: 
        #Concatinate lists
        compsToCompare=[]
        for l in args.comps:
            compsToCompare+=l
    else:
        compsToCompare=None

    for f in (fNameRef,fNameChk):
        if not os.access(f,os.R_OK):
            sys.stderr.write("No read access to file %s\n" % f)
            sys.exit(-1)

    print ('Reference file: ',fNameRef)
    print ('Validation file: ',fNameChk)
    fRef=file(fNameRef)
    fChk=file(fNameChk)
    
    configRef=pickle.load(fRef)
    configChk=pickle.load(fChk)

    compareConfig(configRef,configChk,compsToCompare,args.ignoreMissing,args.perParameterPrint, args.ignoreIdenticalPerParameter, args.ignoreIrrelevant,args.colour)
