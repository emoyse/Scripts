COMPONENT="MuonSegmentMaker"
#COMPONENT="ToolSvc"
#COMPONENT="MuonSegmentMaker.MuonClusterSegmentFinderTool.MuonTrackCleaner_seg"
confTool.py --ignoreDefaults --ignoreIrrelevant --shortenDefaultComponents --ignore MuonIdHelperSvc --ignore edmHelper --ignore DetectorStore -s $COMPONENT def/old-config.pkl > def.config
confTool.py --ignoreDefaults --ignoreIrrelevant --shortenDefaultComponents --ignore MuonIdHelperSvc --ignore edmHelper --ignore DetectorStore -s $COMPONENT ca/config.pkl > ca.config

# Can also do:
# confTool.py --ignoreMissing --ignoreDefaults --ignoreIrrelevant --shortenDefaultComponents --ignore MuonIdHelperSvc --ignore edmHelper --ignore DetectorStore  --includeComps "ToolSvc.*Muon.*" --diff ca/config.pkl def/old-config.pkl > diffconfig.txt
