# Use this as : export PYTHONPATH=<THIS LOCATION>:$PYTHONPATH
#
# athena -i myjobopts.py 
# e.g. athena -i ../runargs.RAWtoESD.py RecJobTransforms/skeleton.RAWtoESD_tf.py
#
# import AthenaComponentDependencies
# alg = getattr(topSequence,"MuGirlStauAlg")
# AthenaComponentDependencies.examine_alg_or_tool(alg,1,{},True,'after.txt')
# tools, graph = examine_alg_or_tool(alg,2,{},True,'after.txt', 'graph.pdf')

from GaudiKernel.Proxy.Configurable import ConfigurableAlgTool
from GaudiKernel.GaudiHandles import PrivateToolHandleArray, PublicToolHandleArray, PublicToolHandle, PrivateToolHandle, ServiceHandle
from AthenaCommon.AppMgr import ToolSvc as toolSvc
from AthenaCommon.AppMgr import topSequence
import pdb, sys

def __print_level(depth, name, type, file, print_to_screen,print_component_type):
  comp_type = ""
  if print_component_type:
    comp_type = type
  s = str(depth)+'     '+' |'*(depth)+str(comp_type)+str(name)
  if (print_to_screen):
    print s
  if file:
      file.write(s+ '\n')

def __get_public_tool(conf):
  if not conf:
      return None
  try:
      tool = getattr( toolSvc, conf.getName() )
      return tool
  except AttributeError:
      return None
  except IndexError:
      return None
      
def examine_all(maxdepth):
  """
  Loop through topSequence to get all configured algs and related tools. 
  Returns a list of all tools, and a subset which is tools which are not explicitly defined:
     all_public_tools, not_defined_public_tools 
  (both are dictionaries with the name of the tool as key, and a list of parents as value)
    
  Example: find out how many clients a public tool has
  export PYTHONPATH=/home/emoyse/Scripts:$PYTHONPATH
  athena -i ../runargs.RAWtoESD.py RecJobTransforms/skeleton.RAWtoESD_tf.py
  
  then at the athena> prompt type:
  import AthenaComponentDependencies
  public_tools, not_defined = AthenaComponentDependencies.examine_all(99)
  public_tools['MuonLayerHoughTool']
  
  prints:
  set(['MuonSegmentFinder', 'MooSegmentFinder_NCB', 'MuonStauRecoTool'])
  """
  # hardcoding for the moment
  maxdepth=9999
  # Firstly, lets loop through all public tools.
  all_public_tools = {}
  not_defined_public_tools={}
  print 'ToolSvc has', len(toolSvc.getAllChildren()), 'tools. Will now loop through them.'
  
  counter =0
  for tool in toolSvc.getAllChildren():   
    # We want to examine all the tools, because we want to work out who links to what.
    
    # First add this tool
    all_public_tools.setdefault(tool.getName(), set())
    
    num_pub_tools = len(all_public_tools)
    # Add its children (by looping through all properties and looking for tools)
    __recurse_alg_or_tool(tool, 0, maxdepth, all_public_tools, not_defined_public_tools, True, True, False, None, None)
    print  counter, tool.getName(), 'added ',len(all_public_tools)-num_pub_tools, 'public tools, (total=',len(all_public_tools),')'
    
    # if tool.getName() is 'XXXInDetPixelRawDataProviderTool':
    #   toolkeys = all_public_tools.keys()
    #   print '**** Debugging with tool = InDetPixelRawDataProviderTool. Have',len(toolkeys),' entries in all_public_tools'
    #
    #   # print 'all_public_tools.keys() :',toolkeys
    #   missing_tool_names=[]
    #
    #   # for tool in toolSvc.getAllChildren():
    #   #   if tool not in tool_names:
    #   #     missing_tool_names.append(tool.getName())
    #   # print 'Known to toolSvc, but not in dictionary: ',missing_tool_names
    #
    #   missing_tool_names=[]
    #   for toolname in toolkeys:
    #     try:
    #       conf_tool = getattr( toolSvc, toolname)
    #     except AttributeError:
    #       missing_tool_names.append(tool)
      
    counter+=1
    # if counter>20:
    #   break
      
  print 'Got back ',len(all_public_tools),'public tools from traversing tools, and ',len(not_defined_public_tools),' undefined tools (i.e. will take C++ defaults)'
  if len(all_public_tools)!=( len(toolSvc.getAllChildren())+len(not_defined_public_tools) ):
      print 'WARNING: The totals do not add up.'
  
  # Check that this looks sane.    
  missing_tool_names=[]
  toolkeys = all_public_tools.keys()
  for tool in toolSvc.getAllChildren():
    if tool.getName() not in toolkeys:
      missing_tool_names.append(tool.getName())
  if len(missing_tool_names):
      print 'WARNING: ',len(missing_tool_names), 'Tools known to ToolSvc, but not in dictionary: ',missing_tool_names

  missing_tool_names=[]
  for toolname in toolkeys:
    try:
      conf_tool = getattr( toolSvc, toolname)
    except AttributeError:
      missing_tool_names.append(toolname)
  
  if len(missing_tool_names):
    print 'INFO: ',len(missing_tool_names), 'Tools known to dictionary, but not in ToolSvc (so will take C++ default): ',missing_tool_names
  
  # return all_public_tools
  
  print 'Will now examine',len(topSequence.getAllChildren()),'Algorithms'
  for alg in topSequence.getAllChildren():
    public_tools = {}
    # Should be no need to examine public tools now, since they're in the dict already
    __recurse_alg_or_tool(alg, 0, maxdepth, public_tools, not_defined_public_tools, True, False, False, None, None)
    # merge results
    for k,v in public_tools.iteritems():
      if k in all_public_tools:
        all_public_tools[k]=all_public_tools[k]|v
      else:
        all_public_tools[k]=v
  print 'Now have',len(all_public_tools),'public tools from traversing algs, of which ',len(not_defined_public_tools),' are undefined tools (i.e. will take default.)'
  return all_public_tools, not_defined_public_tools

def examine_alg_or_tool(alg, maxdepth, examine_public_tools = False, print_to_screen=True, print_component_type = False, file_name=None, graph_name=None):
  """
  Dumps information about a tool/alg and optionally dump public tools too (default is just dump private), output to a text file, and make a graph.

  Example:
  export PYTHONPATH=<THIS LOCATION>:$PYTHONPATH
  athena -i ../runargs.RAWtoESD.py RecJobTransforms/skeleton.RAWtoESD_tf.py
  
  then type:
  import AthenaComponentDependencies
  alg = getattr(topSequence,"MuGirlStauAlg")
  pubtools, graph = AthenaComponentDependencies.examine_alg_or_tool(alg,99,examine_public_tools=True,print_to_screen=False,file_name='after.txt')
  
  For a tool, you can do e.g.
  tool = getattr( ToolSvc, 'MuonAmbiProcessor')
   
  Keyword arguments:
  alg -- An algorithm/tool. You can get e.g. an algorithm instance by 'alg = getattr(topSequence,"MuGirlStauAlg")'
  maxdepth -- the maximum depth to descend in the dependencies. So for example, a depth of 1 will just dump the tools pointed to by alg.
  examine_public_tools -- if True (default is False), look into the configuration of public tools too. Useful for comparing like with like in public to private migrations.
  print_component_type -- if True (default), print e.g. whether this is a public or private. Hide to make diffs smaller.
  file_name -- if set, the text file to dump the output to (e.g. after.txt)
  graph_name -- if set, the name of the pygraphviz graph to generate.

  Returns a set of public tools (where the key is the tool, and the values are all owners of handles that point to it), 
  and a pygraphviz graph (None, if not requested)
  
  For the graph the following colour conventions are used:
    * Purple = service
    * red = public tool, fully defined
    * blue = private tool, fully defined
    * yellow = public tool, undefined
    * magenta = private tool, undefined
    * green = public tool from ToolHandleArray
    * black = private tool from ToolHandleArray
  
  """
  depth = 0  
  public_tools={}
  not_defined_public_tools = {}
  
  if file_name:
      file = open(file_name,'w')
  else:
      file=None
      
  if graph_name:
      print 'Creating dependency graph : ',graph_name
      import pygraphviz as pgv 
      graph = pgv.AGraph()
  else:
      graph=None
  __recurse_alg_or_tool(alg, depth, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph)
  
  print 'Undefined public tools :', not_defined_public_tools
  
  if file:
      file.close()
  if graph is not None:
      graph.layout('dot')
      graph.draw(graph_name)
  return public_tools, graph
  
def __add_tool_to_graph(parent,tool,graph,shape,colour,label):
    graph.add_edge(tool, parent, color=colour, label=label) 
    if graph.has_node(tool):            
        n=graph.get_node(tool)
        n.attr['shape']=shape
    else:
        print 'Something odd has happened. Graph is apparently missing ',tool
        print graph

def __add_public_tool(parent, tool, public_tools, graph, colour, label, print_to_screen=True):
  if not tool:
      if print_to_screen:
        print 'Parent', parent, 'has no tool defined for property', label
      return
  else:
      if print_to_screen:
        print 'Adding pub tool ', tool, ' with parent ', parent
  if type(tool) != type('str'):
    pdb.set_trace()
  public_tools.setdefault(tool, set()).add(parent)   
  if graph is not None:
      __add_tool_to_graph(parent,tool,graph,'box',colour, label)

def __add_private_tool(parent,tool,graph,colour,label, print_to_screen=True):
    if not tool:
        if print_to_screen:
          print 'Parent', parent, 'has no tool defined for property', label
        return
    else:
        if print_to_screen:
          print 'Adding private tool ', tool, ' with parent ', parent
    if graph is not None:
        __add_tool_to_graph(parent,tool,graph,'parallelogram',colour, label)

def __add_service(parent,tool,graph,colour,label, print_to_screen=True):
    if not tool:
        if print_to_screen:
          print 'Parent', parent, 'has no tool defined for property', label
        return
    else:
        if print_to_screen:
          print 'Adding service ', tool, ' with parent ', parent
    if graph is not None:
        __add_tool_to_graph(parent,tool,graph,'parallelogram',colour, label)

def __recurse_alg_or_tool(alg, depth, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph):
  print alg, depth
  # print print_component_type
  if alg.getName() in ['MuonTrackQuery','CombinedMuonTrackBuilder']:
    import pdb ; pdb.set_trace()
    
  if alg.getName() in public_tools:
      print 'Component',alg.getName(), ' already in list of public tools.'
      return

  if depth>maxdepth:
    print ('Maximum depth of ',depth,'reached')
    return
    
  for k,v in alg.properties().items():
    # Normally expect k to be the property name here.
    
    if isinstance(v,ConfigurableAlgTool):
      # This means it was explicitly configured
      if v.isPublic():
        __print_level(depth, k, '[Pub tool] ', file, print_to_screen, print_component_type)
        __add_public_tool(alg.getName(),v.getName(),public_tools,graph, 'red', k, print_to_screen) 
      else:
        __print_level(depth, k, '[Pri tool] ', file, print_to_screen, print_component_type)
        __add_private_tool(alg.getName(),v.getName(),graph, 'blue', k, print_to_screen)
        __recurse_alg_or_tool(v, depth+1, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph)
    elif isinstance(v,PublicToolHandle):
      # This means it's a 'bare' handle, and probably not known to ToolSvc yet
      # Here k is the name of the property
      __print_level(depth, k, '[Pub TH] ', file, print_to_screen, print_component_type) 
      if examine_public_tools:
        pub_tool = __get_public_tool(v) 
        if pub_tool and not pub_tool.getName() in public_tools:
            __recurse_alg_or_tool( pub_tool, depth+1, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph)
        else:
           if v.getName():
             # Bit pointless to print this with no name.
             s = v.getName()+ ' not defined as public tool (so default instance created at runtime).'
             not_defined_public_tools.setdefault(v.getName(), set()).add(alg.getName())   
             if print_to_screen:
               print s 
             if file:
                file.write(s+ '\n') 
      __add_public_tool(alg.getName(),v.getName(),public_tools,graph, 'yellow', k, print_to_screen)       
    elif isinstance(v,PrivateToolHandle):
      __print_level(depth, k, '[Pri TH] ', file, print_to_screen, print_component_type) 
      __add_private_tool(alg.getName(),v.getName(),graph, 'magenta', k, print_to_screen)
            
      # examine_algorithm(v, depth+1)
    elif isinstance(v,PublicToolHandleArray):
      __print_level(depth, k, '[Pub TH array] ', file, print_to_screen, print_component_type) 
      for tool in v:
        __add_public_tool(alg.getName(),tool.getName(),public_tools,graph, 'green', k, print_to_screen)    
        __print_level(depth+1, tool, '[Pub TH] ', file, print_to_screen, print_component_type) 
        if examine_public_tools:
            pub_tool = __get_public_tool(tool)
            if pub_tool:
                __recurse_alg_or_tool( pub_tool, depth+2, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph)
            else:
               s = tool.getName()+ ' not defined as a public tool (so default instance created at runtime).'
               not_defined_public_tools.setdefault(v.getName(), set()).add(alg.getName())                
               if print_to_screen:
                 print s 
               if file:
                  file.write(s+ '\n') 
    elif isinstance(v,PrivateToolHandleArray):
      for tool in v:          
         if not isinstance(tool,PrivateToolHandle):             
           __recurse_alg_or_tool(tool, depth+1, maxdepth, public_tools, not_defined_public_tools, examine_public_tools, print_to_screen, print_component_type, file, graph)
         else :
           __add_private_tool(alg.getName(),tool.getName(),graph, 'black', k, print_to_screen)    
           __print_level(depth, tool, '[Pri TH] ', file, print_to_screen, print_component_type)   
    elif isinstance(v,ServiceHandle):
      if k not in ('DetStore','EvtStore'):
        __print_level(depth, k, '[Svc]', file, print_to_screen, print_component_type)
        __add_service(alg.getName(),v.getName(),graph, 'purple', k, print_to_screen)    
    else:
        if k not in ('AuditTools', 'AuditRestart', 'OutputLevel', 'ExtraOutputs', 'MonitorService', 'IsIOBound', 'AuditInitialize'):
          s = str(depth)+'     '+' |'*(depth)+str(k)+': '+str(v)
          if print_to_screen:
            print s 
          if file:
             file.write(s+ '\n')
        
      # if not isinstance(v,str) and not isinstance(v,bool) and not isinstance(v,list) and not isinstance(v,float) and not isinstance(v,int):
#         print '|'*(depth-1),'  Unk: ',k, type(v)
  return 

def draw_all_graphs(graph, postfix='.svg'):
  """
  Draws all possible graph types - useful for deciding which works best.
  """
  layouts = ('dot', 'twopi','circo','fdp','sfdp')
  # layouts = ('neato', 'dot', 'twopi', 'circo', 'fdp', 'nop', 'gvpr', 'gvcolor', 'ccomps', 'sccmap', 'tred', 'sfdp', 'unflatten')
  for layout in layouts:
      print 'Drawing', layout
      graph.layout(layout)
      graph.draw(layout+postfix)
        
        